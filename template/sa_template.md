# TITLE HERE

| Version | Date       | Author(s)                           | Remarks           |
|:--------|:-----------|:------------------------------------|:------------------|
| V0.1    | 1 Aug 2020 | Ariyandi  Widarto <br>Wisnu Wardoyo | Initial Documents |



## Requirement
| Title            | Link |
|:-----------------|:-----|
| Product Document | Link |
| UI Design        | Link |

## Reference
| Title                 | Link |
|:----------------------|:-----|
| Other Domain SA       | Link |
| Open Source Knowledge | Link |

## Terminology
| Term    | Descriptions                  |
|:--------|:------------------------------|
| Chicken | One head, two legs, one tail. |

## Overall Solution
_System analysis document overview, which contain the big picture of the changes that will be made based on overall tech solution. May be including the background, chosen solution, and also the overview impact(s) to other module._

### Use Case Diagram
_List down the user case of business and system using use case diagram if needed._

## Modification Plan
_Modification plan overview, which contain a brief explanation about the changes itself._

### System Dependency
_List down external system dependency using component diagram._

### Database Structure Changes
_In this section, the changes for database structure (DDL) should be listed. The description of the changes or the table structure that will be changes can be included in this section._

**table_name**

| Column   | Type    | Length | Nullable | Default        | Remarks                                                 |
|:---------|:--------|:-------|:---------|:---------------|:--------------------------------------------------------|
| user_id  | varchar | 64     | N        |                | User identifier that will be used to identify the user. |
| nickname | varchar | 256    | Y        | Captain Morgan | User's nickname to be called.                           |

### Data Model
_Describe the data model using ER diagram if needed._

### Database Data Changes
_In this section, the changes for database data (DML) should be listed. The description of the changes or the SQL script can be included in this section._
```sql
  INSERT INTO table_name (column_name, column_name) VALUES ('value', 'value');
  UPDATE table_name SET column_name='value' WHERE colum_name='value';
```

### State Machine
_Describe the state definition and change condition._

### Key Process
_Describe the key process inside the domain/system based on the overall solution, sequence diagram (or activity diagram if it's to complex) could be used in this section._

### Service Changes
**service_name**

_This section should be filled with the description of the changes for this service or the description of the new service._

#### Service Defintion
```java
public interface service_name {

  public result_class method_name(request_class request name);

}
```

#### Request Parameter

_This section should be filled with request parameter in table form._

| No | Name        | Type      | Length | Required | Condition                                     | Remarks           |
|:---|:------------|:----------|:-------|:---------|:----------------------------------------------|:------------------|
| 1  | param_name  | String    | 256    | M        |                                               | param description |
| 2  | param_name2 | MoneyView | N/A    | C        | Present if first param meet certain condition | param description |


#### Response Parameter

_This section should be filled with response parameter in table form_

| No | Name        | Type      | Length | Required | Condition                                     | Remarks           |
|:---|:------------|:----------|:-------|:---------|:----------------------------------------------|:------------------|
| 1  | param_name  | String    | 256    | M        |                                               | param description |
| 2  | param_name2 | MoneyView | N/A    | C        | Present if first param meet certain condition | param description |

#### Object Model

_This section should be filled with the object model used in the service parameter._

| No | Name        | Type      | Length | Required | Condition                                     | Remarks           |
|:---|:------------|:----------|:-------|:---------|:----------------------------------------------|:------------------|
| 1  | param_name  | String    | 256    | M        |                                               | param description |
| 2  | param_name2 | MoneyView | N/A    | C        | Present if first param meet certain condition | param description |

#### Error Codes

_This section should be filled with the error code used in the service response._

| No | Error Code | Descriptions      |
|:---|:-----------|:------------------|
| 1  | 1000101001 | Parameter Illegal |


#### Sequence Diagram

_This section should be filled by sequence diagram of service, can be as an image or as a plantuml extension._

### Message Handler Changes

_This section should be filled with message handler changes description and also the sequence diagram that represent the logic changes. Can be included as an image or as a plantuml extenstion._

#### Event definition


| No | Topic Code   | Event Code         | Descriptions       |
|:---|:-------------|:-------------------|:-------------------|
| 1  | TOPIC_S_1234 | EVENTCODE_LOG_0001 | Registration Event |

#### Object Model

_This section should be filled with the object model used in the message parameter._

| No | Name        | Type      | Length | Required | Condition                                     | Remarks           |
|:---|:------------|:----------|:-------|:---------|:----------------------------------------------|:------------------|
| 1  | param_name  | String    | 256    | M        |                                               | param description |
| 2  | param_name2 | MoneyView | N/A    | C        | Present if first param meet certain condition | param             |


#### Sequence Diagram

_This section should be filled by sequence diagram of message handler, can be as an image or as a plantuml extension._

### Configuration Changes

_This section should be filled with configuration changes, e.g. virtual table changes, Message Publish and Subscribe, Database Rule, Parameter, etc_

| Type           | Remarks | Before | After |
|:---------------|:--------|:-------|:------|
| Message Broker |         |        |       |
| Database       |         |        |       |
| Resource       |         |        |       |
| Parameter      |         |        |       |

#### Other Changes

_This section should be filled with other changes inside the module, e.g. enum changes, code refactor, etc._

### Downstream System

_In this section, if you have change proposal to downstream system or other team you can list out here._


### Compatibility analysis
_Put your compatibility issues or scenario here and how to overcome it_

## Task Allocation

| No | Task   | Breakdown   | Estimation | Priority | Assignee |
|:---|:-------|:------------|:-----------|:---------|:---------|
| 1  | Task#1 | Breakdown#1 | 1md        | P0       | Iron Man |
