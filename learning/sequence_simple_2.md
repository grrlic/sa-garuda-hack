```plantuml

actor user
entity web

participant "Gateway" as gate
participant "On Boarding\nSystem" as ob
participant "Security System" as sec

activate user

user -> web+: open landing page\nclick register
web -> gate+: HTTP call
gate -> ob+: open register page
return register page\nand data
return register page
return render page

user -> web+: submit data
web -> gate+: HTTP call
gate -> ob+: submit register data

alt if OTP = null | securityID = null
  ob -> sec+: send OTP
  return send result
else
  ob -> sec+: verify OTP
  return verify status

  ob -> ob: save data to DB
end


return register status
return register status
return render info

deactivate user

```
