```plantuml

Title System Dependency

package "Onboading" {
    component "On Boading System" as ob
    interface register
    interface login

}

package "Security" {
    component "OTP System" as otp
    interface sendOTP
    interface verifyOTP

}

package "Notification" {
   component "SMS Provider" as sms
   interface sendSMS
}


'interface publish
ob -up- register
ob -up- login

otp -up- sendOTP
otp -up- verifyOTP

sms -left- sendSMS


'interface consume
HTTP --> register
HTTP --> login

ob --> sendOTP
ob --> verifyOTP

otp -right-> sendSMS


'hidden
Notification -[hidden]left-> Security
```
